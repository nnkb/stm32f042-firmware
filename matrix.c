/*
Copyright 2012 Jun Wako <wakojun@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ch.h"
#include "hal.h"

/*
 * scan matrix
 */
#include "print.h"
#include "debug.h"
#include "util.h"
#include "matrix.h"
#include "wait.h"

#ifndef DEBOUNCE
#   define DEBOUNCE 5
#endif
static uint8_t debouncing = DEBOUNCE;

/* matrix state(1:on, 0:off) */
static matrix_row_t matrix[MATRIX_ROWS];
static matrix_row_t matrix_debouncing[MATRIX_ROWS];

static matrix_row_t read_cols(void);
static void init_cols(void);
static void unselect_rows(void);
static void select_row(uint8_t row);

const ioline_t cols[MATRIX_COLS] = {
  PAL_LINE(GPIOA, 1),  
  PAL_LINE(GPIOA, 0),  
  PAL_LINE(GPIOF, 1),  
  PAL_LINE(GPIOB, 12),
  PAL_LINE(GPIOB, 13),
  PAL_LINE(GPIOB, 14),
  PAL_LINE(GPIOB, 15),
  PAL_LINE(GPIOA, 8),
  PAL_LINE(GPIOA, 9),
  PAL_LINE(GPIOA, 10),
  PAL_LINE(GPIOA, 15),
  PAL_LINE(GPIOB, 3),
  PAL_LINE(GPIOB, 4),
  PAL_LINE(GPIOB, 5),
  PAL_LINE(GPIOB, 8),
  PAL_LINE(GPIOB, 9)
};

const ioline_t rows[MATRIX_ROWS] = {
  PAL_LINE(GPIOF, 0),
  PAL_LINE(GPIOC, 15),
  PAL_LINE(GPIOC, 14),
  PAL_LINE(GPIOC, 13),
  PAL_LINE(GPIOB, 0)
};

inline
uint8_t matrix_rows(void)
{
    return MATRIX_ROWS;
}

inline
uint8_t matrix_cols(void)
{
    return MATRIX_COLS;
}

#define LED_ON()    do { palSetPad(GPIOI, 1U) ;} while (0)
#define LED_OFF()   do { palClearPad(GPIOI, 1U); } while (0)
#define LED_TGL()   do { palTogglePad(GPIOI, 1U); } while (0)

void matrix_init(void)
{ 
    // initialize row and col
    for (int i = 0; i<MATRIX_ROWS; i++) {
      palSetLineMode(rows[i], PAL_MODE_OUTPUT_PUSHPULL);
    }
    unselect_rows();
    init_cols();

    // initialize matrix state: all keys off
    for (uint8_t i=0; i < MATRIX_ROWS; i++) {
        matrix[i] = 0;
        matrix_debouncing[i] = 0;
    }
}

uint8_t matrix_scan(void)
{
    for (uint8_t i = 0; i < MATRIX_ROWS; i++) {
        select_row(i);
        wait_us(30);  // without this wait read unstable value.
        matrix_row_t cols = read_cols();
        if (matrix_debouncing[i] != cols) {
            matrix_debouncing[i] = cols;
            if (debouncing) {
                debug("bounce!: "); debug_hex(debouncing); debug("\n");
            }
            debouncing = DEBOUNCE;
        }
        unselect_rows();
    }

    if (debouncing) {
        if (--debouncing) {
            wait_ms(1);
        } else {
            for (uint8_t i = 0; i < MATRIX_ROWS; i++) {
                matrix[i] = matrix_debouncing[i];
            }
        }
    }

    return 1;
}

inline
bool matrix_is_on(uint8_t row, uint8_t col)
{
    return (matrix[row] & ((matrix_row_t)1<<col));
}

inline
matrix_row_t matrix_get_row(uint8_t row)
{
    return matrix[row];
}

void matrix_print(void)
{
    print("\nr/c 0123456789ABCDEF\n");
    for (uint8_t row = 0; row < MATRIX_ROWS; row++) {
        phex(row); print(": ");
        pbin_reverse16(matrix_get_row(row));
        print("\n");
    }
}

/* Column pin configuration
 */
static void  init_cols(void)
{
  for (int i = 0; i<MATRIX_COLS; i++) {
    palSetLineMode(cols[i], PAL_MODE_INPUT_PULLUP);
  }
}

/* Returns status of switches(1:on, 0:off) */
static matrix_row_t read_cols(void)
{ 
  int res = 0;
  for (int i = 0; i<MATRIX_COLS; i++) {
    res |= ((palReadLine(cols[i]) == PAL_HIGH) ? 0 : (1<<i));
  }

  return res;
}

/* Row pin configuration
 */
static void unselect_rows(void)
{
  for (int i = 0; i<MATRIX_ROWS; i++) {
    palSetLine(rows[i]);
  }
}

static void select_row(uint8_t row)
{
    palClearLine(rows[row]);
}
